<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GradeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\StudentController;
use App\Http\Middleware\CheckLogin;

Route::get('/login', [LoginController::class, "login"])->name("login");
Route::post('/login-process', [LoginController::class, "loginProcess"])->name("login-process");

Route::middleware([CheckLogin::class])->group(function () {
    Route::get('/logout', [LoginController::class, "logout"])->name('logout');

    Route::get('/', function () {
        return view('welcome');
    })->name('welcome');

    // Quản lý lớp => CRUD 
    Route::resource('grade', GradeController::class);

    // Quản lý sinh viên => CRUD 
    Route::resource('student', StudentController::class);
});
