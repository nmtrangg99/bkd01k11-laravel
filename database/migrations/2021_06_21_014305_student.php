<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Student extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student', function (Blueprint $table) {
            // $table->id(); # tạo id và primary key vừa auto_increment: kiểu dữ liệu bigInteger & unsigned
            $table->increments('idStudent'); # tạo id và primary key vừa auto_increment: kiểu dữ liệu big & unsigned
            $table->string('firstName', 50); # varchar => 255 để nguyên
            $table->string('lastName', 50); # varchar => 255 để nguyên
            $table->boolean('gender');
            $table->date('dateBirth');
            // $table->timestamps(); # tự động tạo 2 cột là created_at và updated_at
            $table->unsignedInteger('idGrade');
            $table->foreign('idGrade')->references('idGrade')->on('grade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('student');
    }
}
