<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Đăng nhập</title>
</head>

<body>
    <h1>Đăng nhập</h1>
    <form action="{{ route('login-process') }}" method="post">
        @csrf
        Email: <input type="text" name="email" required>
        <br>
        Password: <input type="password" name="password" required>
        <br>
        <button>Đi zô</button> <br>
        @if (Session::exists('error'))
            <div>{{ Session::get('error') }}</div>
        @endif
    </form>
</body>

</html>
