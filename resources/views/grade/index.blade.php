@extends('layout.app')

@section('content')
    <h1>Danh sách love</h1>
    <a href="{{ route('grade.create') }}">Thêm love</a>

    <form action="" method="get">
        <input type="text" name="search" value="{{ $search }}">
        <button>Tìm nó</button>
    </form>

    <table>
        <tr>
            <th>Mã</th>
            <th>Tên</th>
            <th>Xem</th>
            <th>Sửa</th>
            <th>Xóa</th>
        </tr>
        @foreach ($listGrade as $grade)
            <tr>
                <td>{{ $grade->idGrade }}</td>
                <td>{{ $grade->nameGrade }}</td>
                <td><a href="{{ route('grade.show', $grade->idGrade) }}">Xem</a></td>
                <td></td>
                <td></td>
            </tr>
        @endforeach
    </table>

    {{ $listGrade->appends([
        'search' => $search,
    ])->links() }}
@endsection
