@extends('layout.app')

@section('content')
    <table>
        <tr>
            <th>Mã</th>
            <th>Họ Tên</th>
            <th>Giới tính</th>
            <th>Ngày sinh</th>
            <th>Lớp</th>
            <th>Sửa</th>
            <th>Xóa</th>
        </tr>
        @foreach ($listStudent as $student)
            <tr>
                <td>{{ $student->idStudent }}</td>
                <td>{{ $student->FullName }}</td>
                <td>{{ $student->GenderName }}</td>
                <td>{{ $student->dateBirth }}</td>
                <td>{{ $student->idGrade }}</td>
                <td>
                    <a href="{{ route('student.edit', $student->idStudent) }}">
                        Sửa
                    </a>
                </td>
                <td>
                    <form action="{{ route('student.destroy', $student->idStudent) }}" method="post">
                        @method('DELETE')
                        @csrf
                        <button>Xóa</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
@endsection
