@extends('layout.app')

@section('content')
    <h1>Sửa điiiii</h1>

    <form action="{{ route('student.update', $student->idStudent) }}" method="post">
        @method('PUT')
        @csrf
        Tên: <input type="text" name="first-name" value="{{ $student->firstName }}">
        <br>
        Họ: <input type="text" name="last-name" value="{{ $student->lastName }}">
        <br>
        Giới tính: <input type="radio" name="gender" value="1" @if ($student->gender == 1) checked @endif>
        Nam
        <input type="radio" name="gender" value="0" @if ($student->gender == 0) checked @endif>
        Nữ
        <br>
        Ngày sinh: <input type="date" name="date-birth" value="{{ $student->dateBirth }}">
        <br>
        Lớp: <select name="id-grade">
            @foreach ($listGrade as $grade)
                <option value="{{ $grade->idGrade }}" @if ($student->idGrade == $grade->idGrade) selected @endif>
                    {{ $grade->nameGrade }}
                </option>
            @endforeach
        </select>
        <br>
        <button>Ok</button>
    </form>

@endsection
