<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->session()->exists('admin')) {  // Nếu có session 
            return $next($request);
        } else { // Nếu không tồn tại 
            return Redirect::route('login')->with("error", "Chưa đăng nhập đã đòi vào");
        }
    }
}
