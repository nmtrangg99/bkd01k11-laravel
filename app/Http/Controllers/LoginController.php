<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    public function login()
    {
        return view("login");
    }

    public function loginProcess(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');
        try {
            $admin = Admin::where('email', $email)->where('password', $password)->firstOrFail();
            $request->session()->put('admin', $admin->idAdmin);
            return Redirect::route('welcome');
        } catch (Exception $e) {
            return Redirect::route('login')->with('error', 'Sai dữ liệu gòiiii');
        }
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return Redirect::route('login');
    }
}
