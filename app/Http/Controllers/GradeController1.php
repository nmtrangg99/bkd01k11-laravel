<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Grade;

class GradeController1 extends Controller
{
    // Hiển thị form thêm lớp
    public function create()
    {
        return view('grade.create');
    }

    public function store(Request $request)
    {
        // Nhận dữ liệu
        $name = $request->get('name');
        // DB::table('grade')->insert([
        //     "nameGrade" => $name,
        // ]);
        $grade = new Grade();
        $grade->nameGrade = $name;
        $grade->save();
    }
}
