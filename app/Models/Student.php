<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $table = 'student';

    public $timestamps = false;

    public $primaryKey = 'idStudent';

    public function getGenderNameAttribute()
    {
        if ($this->gender == 0) {
            return 'Nữ';
        } else {
            return 'Nam';
        }
    }

    public function getFullNameAttribute()
    {
        return $this->lastName . ' ' . $this->firstName;
    }
}
